package com.sea.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @program: pdf
 * @author: zhaoHaoKun
 * @Time: 2024/1/2 0002  下午 14:03
 * @description:
 */
public class GeneratePdf {
    /**
     * HTML 转 PDF
     *
     * @param content html内容
     * @param outPath 输出pdf路径
     * @return 是否创建成功
     */
    public static boolean html2Pdf(String content, String outPath) throws Exception {
        Document document = new Document(); //创建一个标准的A4纸文档
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(outPath));//书写器与ducument文档关联
            document.open();//打开文档
            XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                    new ByteArrayInputStream(content.getBytes()), null, Charset.forName("UTF-8"));
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            document.close();//关闭文档
        }
    }

    /**
     * 获取模板内容
     * @param templateDirectory 模板文件夹
     * @param templateName      模板文件名
     * @param paramMap          模板参数
     * @return
     * @throws Exception
     */
    public static String getTemplateContent(String templateDirectory, String templateName, Map<String, Object> paramMap) throws Exception {
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);//不兼容配置
        if (templateDirectory.startsWith("!")) {
            configuration.setClassForTemplateLoading(GeneratePdf.class, templateDirectory.replace("!", "/"));
        } else {
            configuration.setDirectoryForTemplateLoading(new File(templateDirectory));//加载模板
        }
        try (Writer out = new StringWriter()) {
            Template template = configuration.getTemplate(templateName, "UTF-8");//缓存
            template.process(paramMap, out);
            out.flush();
            out.close();
            return out.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean createPdfByDocTemplate(Map<String, Object> dataMap, String htmlDir,String htmlFile, String newFilePath) throws Exception {
        String htmlContent = GeneratePdf.getTemplateContent(htmlDir, htmlFile, dataMap);
        return GeneratePdf.html2Pdf(htmlContent, newFilePath);
    }
}
